You will need to install [nanomsg](https://nanomsg.org) on you machine
This project also assumes a running Kafka / zookeeper instance on the default ports `2181` & `9092` and a `chat` topic set up
```
npm install
bower install
node index.js
```

[http://0.0.0.0:3000](http://0.0.0.0:3000)
