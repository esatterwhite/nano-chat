/*jshint esnext: true, node: true, laxcomma: true, smarttabs: true */
'use strict';

let kafka = require('kafka-node')
  , connection = new kafka.Client()
  ;

module.exports = function( topic = 'chat', partition = 0, cb ){
    let consumer = new kafka.HighLevelConsumer( connection,[ { topic, partition } ] );
    consumer.on('error',( err )=>{
        console.log(err);
        process.exit(1)
    })
    console.log( arguments );
    cb( null, consumer );
}
