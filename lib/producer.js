/* jshint esnext: true, node: true, smarttabs: true, laxcomma: true */
'use strict';
let kafka = require('kafka-node')
  , connection = new kafka.Client(null,null,{})
  ;

module.exports = function( cb ){
    let producer = new kafka.HighLevelProducer( connection,{partitionerType:0});
    producer.on('ready', ( )=>{
        cb( null, producer );
    });
};


