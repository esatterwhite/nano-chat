/*jshint esnext: true, node: true, laxcomma: true, smarttabs: true*/
'use strict';

const nanomsg = require('nanomsg');
const conf = require('keef');
const pair = nanomsg.socket('pub');
const Producer = require('./lib/producer');
const Consumer = require('./lib/consumer');
const fs = require('fs');
const path = require('path');
const express = require('express');
const app = require('express')();

// open a websocket
let address = `ws://${conf.get('host')}:${~~conf.get('PORT') + 1}`;
console.log( address );
pair.bind(address);
app.set('x-powered-by',false);

// start a kafka consumer to push data
Consumer( conf.get('kafka:topic'), conf.get('kafka:part'), ( err, consumer )=>{

    // when we get a message push it down the socket
    consumer.on('message', function( msg ){
        console.log('message: %s', msg.value );
        pair.send( msg.value);
    });

    Producer( ( perr, producer )=>{
        let topic = conf.get('kafka:topic');
        let partition  = conf.get('kafka:part');
        let attributes = 2;
        
        app.post('/', (req, res)=>{

            let messages = req.body.message;
            producer.send([
                {topic, messages, attributes, partition }
            ], function(err){

                if( err ){
                    res.status( 500 );
                    res.end( err.message );
                    return console.log( err );
                }

                res.status(200);
                res.end();
            } );
            
        });
        producer.on('error', function( err ){
            console.error( err );
            process.exit(1);
        });
    });
});

app.use( '/static', express.static( path.join( __dirname, 'static') ) );
app.use( '/elements', express.static( path.join( __dirname, 'elements') ) );
app.use( '/components', express.static( path.join( __dirname, 'bower_components') ) );
app.use( require('body-parser').json() );
app.get('/', (req, res)=>{
    fs.createReadStream('index.html').pipe( res );
});

app.listen(3000);
